﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using Newtonsoft.Json;

namespace WinForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            InitializeChromium();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }


        public ChromiumWebBrowser chromeBrowser;

        public void InitializeChromium()
        {
            //------------------------------------------------------------
            //var content = new System.Net.Http.FormUrlEncodedContent(new[]
            //{
            //    new KeyValuePair<string, string>("username", "u1")
            //});

            
            System.Net.Http.HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Router").Result;
            var response_ = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().GetAwaiter().GetResult()); 
            //------------------------------------------------------------
            CefSettings settings = new CefSettings();
            
            // Initialize cef with the provided settings
            Cef.Initialize(settings);
            // Create a browser component
            chromeBrowser = new ChromiumWebBrowser(response_.ToString());
            // Add it to the form and fill it to the form window.
            this.Controls.Add(chromeBrowser);


            chromeBrowser.AddressChanged += Chrome_AddressChanged;
            chromeBrowser.Dock = DockStyle.Fill;
        }




        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Cef.Shutdown();
        }

        private void Chrome_AddressChanged(object sender, AddressChangedEventArgs e)
        {
            //Update url in multiple thread
            this.Invoke(new MethodInvoker(() =>
            {
                txtUrl.Text = e.Address;
            }));
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            chromeBrowser.Refresh();
        }

        private void Go_Click(object sender, EventArgs e)
        {
            chromeBrowser.Load(txtUrl.Text);
        }

        private void Next_Click(object sender, EventArgs e)
        {
            if (chromeBrowser.CanGoForward)
                chromeBrowser.Forward();
        }

        private void Back_Click(object sender, EventArgs e)
        {
            if (chromeBrowser.CanGoBack)
                chromeBrowser.Back();
        }
    }


  

}
