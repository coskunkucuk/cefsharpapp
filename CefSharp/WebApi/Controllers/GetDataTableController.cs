﻿using Newtonsoft.Json;
using System.Web.Http;

namespace WebApi.Controllers
{
    public class GetDataTableController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Get()
        {
            var JsonString = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(@"~/Models/Data.json"));
            var JsonObject = JsonConvert.DeserializeObject(JsonString);
            return Ok(JsonObject);
        }
    }
}