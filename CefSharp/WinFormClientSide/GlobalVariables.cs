﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace WinFormClientSide
{
    public class GlobalVariables
    {
        public static HttpClient WebApiClient = new HttpClient();
        static GlobalVariables()
        {

            
            WebApiClient.BaseAddress = new Uri("https://localhost:44371/api/");
            //WebApiClient.Timeout = new TimeSpan(0, 0, REQUEST_TIMEOUT_S); 
            WebApiClient.DefaultRequestHeaders.Clear();
            WebApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json")); //ACCEPT header
            //WebApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/html"));
            WebApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded")); //ACCEPT header
            System.Net.ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            // Alınan Token buraya !
            // Fakat Belirli token kontrolleri yapıp gerekli durumlarda refresh token yapılması gerekiyor. bunun kontrolleri yapılması gerek.
            //WebApiClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(at.GetAccessToken());

        }
    }
}