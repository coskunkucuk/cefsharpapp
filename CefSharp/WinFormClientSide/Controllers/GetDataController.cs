﻿using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace WinFormClientSide.Controllers
{
    public class GetDataController : ApiController
    {
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetTable(DevExtreme.AspNet.Mvc.DataSourceLoadOptions loadOptions)
        {
            IEnumerable<Models.DataTableModel> DataModel;
            System.Net.Http.HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("GetDataTable").Result;
            DataModel = response.Content.ReadAsAsync<IEnumerable<Models.DataTableModel>>().Result;
            return Request.CreateResponse(DevExtreme.AspNet.Data.DataSourceLoader.Load(DataModel, loadOptions));
        }
    }
}